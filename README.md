# NebDash

- [Development Environment](https://bitbucket.org/tachyon_go/nebula_dash/wiki/Development%20Environment)
- [Code Style Guidelines](https://bitbucket.org/tachyon_go/nebula_dash/wiki/Code%20Style%20Guidelines)
- [Art Asset Guidelines](https://bitbucket.org/tachyon_go/nebula_dash/wiki/Art%20Asset%20Guidelines)

# Technical Documentation

- [Game Architecture](https://bitbucket.org/tachyon_go/nebula_dash/wiki/Game%20Architecture)
- [Service Reference](https://bitbucket.org/tachyon_go/nebula_dash/wiki/Service%20Reference)
- [Interface Reference](https://bitbucket.org/tachyon_go/nebula_dash/wiki/Interface%20Reference)
- [Enum Reference](https://bitbucket.org/tachyon_go/nebula_dash/wiki/Enum%20Reference)
- [Class Index](https://bitbucket.org/tachyon_go/nebula_dash/wiki/Class%20Index)
    - [Template](https://bitbucket.org/tachyon_go/nebula_dash/wiki/Class%20Template)

# Design Documentation

Design documentation is handled on [Google Drive](https://drive.google.com/drive/u/0/folders/0B2Tcr0yGSOt1eDgtbERUYkpLalU). The things on this wiki are limited to non-design documentation.