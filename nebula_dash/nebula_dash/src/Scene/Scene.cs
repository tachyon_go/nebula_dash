#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Scenes (aka "Screens") hold objects of the game, and are used to make each "section" of the game (main menu, levels,
    /// etc)
    /// </summary>
    public abstract class Scene
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        protected Scene()
        {
            InstanceID = 0;
            Name = "";
            Exiting = false;
            TransitionPosition = 0.0f;
            TransitionOnTime = TimeSpan.Zero;
            TransitionOffTime = TimeSpan.Zero;
            InitialCamPos = Vector2.Zero;
            InitialCamZoom = 0.0f;
            InitialCamRotation = 0.0f;
            InitialCamBoundaryMin = Vector2.Zero;
            InitialCamBoundaryMax = Vector2.Zero;
            SceneState = SceneTransitionState.Hidden;
            SceneCamera = new Camera();
            RemoveMeFromList = false;
        }
        #endregion

        #region Properties
        /// <summary>
        /// UniqueID assigned by a <see cref="SceneManager" />
        /// </summary>
        public int InstanceID { get; set; }

        /// <summary>
        /// Scene's friendly name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// If true, <see cref="SceneState" /> is set to TransitionOff and the Scene begins its logic for going away
        /// </summary>
        public bool Exiting { get; set; }

        /// <summary>
        /// Current point in a Scene's transition
        /// </summary>
        public float TransitionPosition { get; set; }

        /// <summary>
        /// <see cref="TransitionOnTime" /> in milliseconds (used for serialization purposes)
        /// </summary>
        public int TransitionOnTimeInt { get; set; }

        /// <summary>
        /// <see cref="TransitionOffTime" /> in milliseconds (used for serialization purposes)
        /// </summary>
        public int TransitionOffTimeInt { get; set; }

        /// <summary>
        /// Value for <see cref="SceneCamera" />.Position at instantiation time
        /// </summary>
        public Vector2 InitialCamPos { get; set; }

        /// <summary>
        /// Value for <see cref="SceneCamera" />.Zoom at instantiation time
        /// </summary>
        public float InitialCamZoom { get; set; }

        /// <summary>
        /// Value for <see cref="SceneCamera" />.Rotation at instantiation time
        /// </summary>
        public float InitialCamRotation { get; set; }

        /// <summary>
        /// Value for <see cref="SceneCamera" />.BoundaryMin at instantiation time
        /// </summary>
        public Vector2 InitialCamBoundaryMin { get; set; }

        /// <summary>
        /// Value for <see cref="SceneCamera" />.BoundaryMax at instantiation time
        /// </summary>
        public Vector2 InitialCamBoundaryMax { get; set; }

        /// <summary>
        /// Defines the Scene's current state of activity
        /// </summary>
        public SceneTransitionState SceneState { get; protected set; }

        /// <summary>
        /// Scene's camera object; you see the Scene through this
        /// </summary>
        public Camera SceneCamera { get; private set; }

        /// <summary>
        /// If true, remove Scene from the stack of active scenes
        /// </summary>
        public bool RemoveMeFromList { get; private set; }

        /// <summary>
        /// How long the Scene takes in a transition on before <see cref="SceneState" /> is Active
        /// </summary>
        protected TimeSpan TransitionOnTime { get; set; }

        /// <summary>
        /// How long the Scene takes in a transition away before <see cref="RemoveMeFromList" /> is set to true
        /// </summary>
        protected TimeSpan TransitionOffTime { get; set; }
        #endregion

        #region Initialization
        /// <summary>
        /// Initialization universal to all scenes
        /// </summary>
        protected virtual void Initialize()
        {
            SetupCamera();
            AddConsoleCommands();
        }
        #endregion

        #region Update Loops
        /// <summary>
        /// Update loop, removes Scenes if they aren't transitioning and are set to Exiting
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// <param name="inputHandler">Detects input from HID</param>
        public virtual void Update(GameTime gameTime, INebulaInputHandlerService inputHandler)
        {
            if (Exiting) {
                SceneState = SceneTransitionState.TransitionOff;

                // Has the scene completed transitioning off? If so, remove it.
                if (!UpdateTransition(gameTime, TransitionOffTime, 1)) {
                    RemoveMeFromList = true;
                }
            } else {
                // Otherwise it's either transitioning on, or active.
                SceneState = UpdateTransition(gameTime, TransitionOnTime, -1)
                    ? SceneTransitionState.TransitionOn : SceneTransitionState.Active;
            }
        }

        /// <summary>
        /// abstract FixedUpdate loop
        /// </summary>
        public abstract void FixedUpdate();

        /// <summary>
        /// abstract Draw loop
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// <param name="spriteBatch">Enables a group of sprites to be drawn using the same settings.</param>
        /// <param name="texContent">Map of all Texture2Ds to be drawn in this System</param>
        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch, Dictionary<string, Texture2D> texContent);
        #endregion

        #region Private Functions
        // Updates TransitionPosition, returns true if a Scene is still a transitioning state
        // When this returns false, the transition is over.
        private bool UpdateTransition(GameTime gameTime, TimeSpan time, int direction)
        {
            var transitionDelta = 0.0f;

            if (time == TimeSpan.Zero) {
                transitionDelta = 1;
            } else {
                transitionDelta = (float)(gameTime.ElapsedGameTime.TotalMilliseconds / time.TotalMilliseconds);
            }

            TransitionPosition += transitionDelta * direction;

            if ((direction < 0) && (TransitionPosition <= 0) ||
                (direction > 0) && (TransitionPosition >= 1)) {
                TransitionPosition = MathHelper.Clamp(TransitionPosition, 0, 1);
                return false;
            }

            return true;
        }

        // Create a CameraFactory and use scene's "InitialCam" values to set up SceneCamera
        private void SetupCamera()
        {
            var cameraFactory = new CameraFactory();

            SceneCamera = cameraFactory.CreateCamera(InitialCamPos, InitialCamZoom, InitialCamRotation,
                InitialCamBoundaryMin, InitialCamBoundaryMax);
        }

        private void AddConsoleCommands()
        {
            NebulaHelper.RemoveConsoleCommands("Scene");
            var devConsole = GameServices.GetService<VosSoft.Xna.GameConsole.GameConsole>();

            devConsole.AddCommand("mc", delegate {
                var inputHandler = GameServices.GetService<INebulaInputHandlerService>();
                NebulaDebug.Log(inputHandler.GetMousePosition(SceneCamera).ToString());
            }, NebulaHelper.GetConsoleCommandHelp("mc"));
        }
        #endregion
    }
}