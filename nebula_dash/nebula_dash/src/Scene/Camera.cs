#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Defines how you see the game world, and provides an interface to translate 
    /// screen coordinates to world coordinates.
    /// </summary>
    public class Camera
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public Camera()
        {
            _scale = 0;
            _zoom = 0;
            _viewPos = Vector2.Zero;
            Position = Vector2.Zero;
            Rotation = 0;
            BoundaryMin = Vector2.Zero;
            BoundaryMax = Vector2.Zero;
        }
        #endregion

        #region Parameterized Constructors
        /// <summary>
        /// Instantiate a new Camera object
        /// </summary>
        /// <param name="viewPos">View position value used for world -> screen matrix translation</param>
        /// <param name="scale">Scaling value for world -> screen matrix translation</param>
        public Camera(Vector2 viewPos, float scale)
            : this()
        {
            _viewPos = viewPos;
            _scale = scale;
        }
        #endregion

        #region Fields
        private Vector2 _viewPos;
        private readonly float _scale;
        private float _zoom;
        #endregion

        #region Properties
        /// <summary>
        /// Camera's current position in world coordinates
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        /// Radian rotation of the camera
        /// </summary>
        public float Rotation { get; set; }

        /// <summary>
        /// Camera's hard-limit minimum position values in world coordinates
        /// </summary>
        public Vector2 BoundaryMin { get; set; }

        /// <summary>
        /// Camera's hard-limit maximum position values in world coordinates
        /// </summary>
        public Vector2 BoundaryMax { get; set; }

        /// <summary>
        /// Camera's zoom
        /// </summary>
        public float Zoom {
            get { return _zoom; }
            set {
                _zoom = value;
                // prevent negative zoom
                if (_zoom < 0.00f) {
                    _zoom = 0.00f;
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Return a matrix that transforms between world space and screen space
        /// </summary>
        /// <param name="paraX">Parallax scrolling speed on X axis</param>
        /// <param name="paraY">Parallax scrolling speed on Y axis</param>
        /// <returns></returns>
        public Matrix GetViewMatrix(float paraX = 1.0f, float paraY = 1.0f)
        {
            Matrix mat = Matrix.CreateTranslation(-Position.X * paraX, -Position.Y * paraY, 0.0f) *
                         Matrix.CreateRotationZ(Rotation) * Matrix.CreateScale(_scale * _zoom) *
                         Matrix.CreateTranslation(_viewPos.X, _viewPos.Y, 0.0f);

            return mat;
        }

        /// <summary>
        /// Enforces the BoundaryMin and BoundaryMax properties
        /// </summary>
        public void ClampCameraBoundary()
        {
            var cameraClampValue =
                new Vector2(MathHelper.Clamp(Position.X, BoundaryMin.X, BoundaryMax.X),
                    MathHelper.Clamp(Position.Y, BoundaryMin.Y, BoundaryMax.Y));

            Position = cameraClampValue;
        }

        /// <summary>
        /// Focus the camera on an object in the game world
        /// </summary>
        /// <param name="pos">Position of object to focus on</param>
        public void LookAt(Vector2 pos)
        {
            Position = pos;
        }
        #endregion
    }
}