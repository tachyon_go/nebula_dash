#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Instantiate camera objects, usually done from a <see cref="Scene" /> level
    /// </summary>
    public class CameraFactory
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public CameraFactory() {}
        #endregion

        #region Public Functions
        /// <summary>
        /// Returns a new <see cref="Camera" /> object with some defaults initialized from method params
        /// </summary>
        /// <param name="position">Camera's initial position</param>
        /// <param name="zoom">Camera's initial zoom</param>
        /// <param name="rotation">Camera's intiial rotation</param>
        /// <param name="boundMin">Camera'is initial minimum position limit in world coordinates</param>
        /// <param name="boundMax">Camera's initiam maximum position limit in world coordinates</param>
        /// <returns></returns>
        public Camera CreateCamera(Vector2 position, float zoom, float rotation, Vector2 boundMin,
            Vector2 boundMax)
        {
            const int RESOLUTION_SCALING = 960;

            // Setup initial values for new camera
            Viewport viewport =
                GameServices.GetService<GraphicsDeviceManager>().GraphicsDevice.Viewport;
            var viewPos = new Vector2(viewport.Width * 0.5f, viewport.Height * 0.5f);
            float scale = (float)viewport.Width / RESOLUTION_SCALING;

            var newCamera = new Camera(viewPos, scale) {
                Position = position, Zoom = zoom, Rotation = rotation, BoundaryMin = boundMin,
                BoundaryMax = boundMax
            };

            return newCamera;
        }
        #endregion
    }
}