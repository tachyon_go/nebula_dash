#region Using Statements
using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Scene object representing the game world itself; a level.
    /// </summary>
    public sealed class MapScene : Scene
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapScene()
        {
            _entManager = new EntityManager();
            _drawSystem = new DrawSystem();
            ParallaxLayers = new List<Vector2>();
            EntityCollection = new List<Entity>();
        }
        #endregion

        #region Parameterized Constructors
        /// <summary>
        /// Instantiate a new Map from an ID
        /// </summary>
        /// <param name="mapID">ID of scene to spawn, corresponding to values in map_data file</param>
        public MapScene(string mapID)
            : this()
        {
            var mapSceneLUT =
                JsonConvert.DeserializeObject<Dictionary<string, MapScene>>(
                    File.ReadAllText("data/map_data.neb"));

            Name = mapSceneLUT[mapID].Name;
            TransitionOnTime = TimeSpan.FromMilliseconds(mapSceneLUT[mapID].TransitionOnTimeInt);
            TransitionOnTime = TimeSpan.FromMilliseconds(mapSceneLUT[mapID].TransitionOffTimeInt);
            InitialCamPos = mapSceneLUT[mapID].InitialCamPos;
            InitialCamZoom = mapSceneLUT[mapID].InitialCamZoom;
            InitialCamRotation = mapSceneLUT[mapID].InitialCamRotation;
            InitialCamBoundaryMin = mapSceneLUT[mapID].InitialCamBoundaryMin;
            InitialCamBoundaryMax = mapSceneLUT[mapID].InitialCamBoundaryMax;
            ParallaxLayers = new List<Vector2>(mapSceneLUT[mapID].ParallaxLayers);
            EntityCollection = new List<Entity>(mapSceneLUT[mapID].EntityCollection);
            Initialize();
        }
        #endregion

        #region Fields
        private readonly EntityManager _entManager;
        private readonly DrawSystem _drawSystem;
        #endregion

        #region Properties
        /// <summary>
        /// List of values corresponding to parallax scroll speeds
        /// </summary>
        public List<Vector2> ParallaxLayers { get; set; }

        /// <summary>
        /// Active entities at map spawn time; DO NOT USE after map is already setup, use
        /// ActiveEntities property on <see cref="_entManager" /> instead.
        /// </summary>
        public List<Entity> EntityCollection { get; }
        #endregion

        #region Initialization
        /// <summary>
        /// Sets up spawn-time values for the map
        /// </summary>
        protected override void Initialize()
        {
            _entManager.SpawnEntitiesFromList(EntityCollection);
#if DEBUG
            DebugInitialization();
#endif
            base.Initialize();
        }
        #endregion

        #region Update Loops
        /// <summary>
        /// Update loop
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="inputHandler">Detects input from HID</param>
        public override void Update(GameTime gameTime, INebulaInputHandlerService inputHandler)
        {
            _entManager.RemoveMarkedEntities();

            var drawableEntities = new List<Entity>();

            foreach (Entity ent in _entManager.ActiveEntities) {
                if (ent.PositionComponent != null) {
                    if (ent.DrawComponent != null) {
                        drawableEntities.Add(ent);
                    }
                }
            }

            if (ParallaxLayers.Count > 0) {
                _drawSystem.Update(_entManager.ActiveEntities, ParallaxLayers.Count);
            }
            base.Update(gameTime, inputHandler);
        }

        /// <summary>
        /// FixedUpdate loop
        /// </summary>
        public override void FixedUpdate()
        {
            _drawSystem.FixedUpdate(_entManager.ActiveEntities);
        }

        /// <summary>
        /// Draw loop
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// <param name="spriteBatch">Enables a group of sprites to be drawn using the same settings.</param>
        /// <param name="texContent">Map of all Texture2Ds to be drawn in this System</param>
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch, Dictionary<string, Texture2D> texContent)
        {
            GameServices.GetService<GraphicsDeviceManager>().GraphicsDevice.Clear(Color.DimGray);
            _drawSystem.Draw(spriteBatch, texContent, SceneCamera, ParallaxLayers);
        }
        #endregion

        #region Debugging
#if DEBUG
        private void DebugInitialization()
        {
            AddConsoleCommands();
        }

        private void AddConsoleCommands()
        {
            NebulaHelper.RemoveConsoleCommands("MapScene");
            var devConsole = GameServices.GetService<VosSoft.Xna.GameConsole.GameConsole>();

            devConsole.AddCommand("get", delegate(object obj,
                VosSoft.Xna.GameConsole.CommandEventArgs e) {
                var nebulaDebug = GameServices.GetService<INebulaDebugService>();

                if (e.Args.Length > 0) {
                    int number;
                    bool result = int.TryParse(e.Args[0], out number);
                    // if the user passed an int, assume it's instanceID and make a list containing that entity
                    if (result) {
                        var l = new List<Entity>();
                        foreach (Entity ent in _entManager.ActiveEntities) {
                            if (ent.InstanceID == number) {
                                l.Add(ent);
                            }
                        }
                        if (e.Args.Length >= 2) {
                            int n;
                            bool r = int.TryParse(e.Args[1], out n);
                            if (r) {
                                nebulaDebug.PrintEntities(l, n);
                            } else if (e.Args[1] == "p") {
                                nebulaDebug.PrintEntities(l, true, true);
                            }
                        } else {
                            nebulaDebug.PrintEntities(l, true);
                        }
                    } else {
                        switch (e.Args[0]) {
                            case "component":
                            case "c":
                                if (e.Args.Length >= 2 && e.Args[1] == "properties" ||
                                    e.Args.Length >= 2 && e.Args[1] == "p") {
                                    nebulaDebug.PrintEntities(_entManager.ActiveEntities, true,
                                        true);
                                } else {
                                    nebulaDebug.PrintEntities(_entManager.ActiveEntities, true);
                                }
                                break;
                            case "cp":
                                nebulaDebug.PrintEntities(_entManager.ActiveEntities, true, true);
                                break;
                        }
                    }
                } else {
                    nebulaDebug.PrintEntities(_entManager.ActiveEntities);
                }
            }, NebulaHelper.GetConsoleCommandHelp("get"));

            devConsole.AddCommand("set", delegate(object obj,
                VosSoft.Xna.GameConsole.CommandEventArgs e) {
                    var nebulaDebug = GameServices.GetService<INebulaDebugService>();
                foreach (Entity ent in _entManager.ActiveEntities) {
                    if (ent.InstanceID == int.Parse(e.Args[0])) {
                        IEnumerable<INebulaComponent> componentList =
                            nebulaDebug.CheckEntityComponents(ent);

                        foreach (INebulaComponent component in componentList) {
                            if (component.InstanceID == int.Parse(e.Args[1])) {
                                var pList = new List<System.Reflection.PropertyInfo>();
                                foreach (System.Reflection.PropertyInfo p in component.GetType().GetProperties()) {
                                    if (p.GetGetMethod().GetParameters().Length == 0) {
                                        pList.Add(p);
                                    }
                                }
                                foreach (System.Reflection.PropertyInfo p in pList) {
                                    if (p.Name == e.Args[2]) {
                                        System.ComponentModel.TypeConverter tc =
                                            System.ComponentModel.TypeDescriptor.GetConverter(p.PropertyType);
                                        object value = tc.ConvertFromString(null,
                                            System.Globalization.CultureInfo.InvariantCulture, e.Args[3]);
                                        p.SetValue(component, value, null);
                                    }
                                }
                            }
                        }
                    }
                }
            }, NebulaHelper.GetConsoleCommandHelp("set"));

            devConsole.AddCommand("spawn", delegate(object obj,
                VosSoft.Xna.GameConsole.CommandEventArgs e) {
                if (e.Args.Length > 0) {
                    if (e.Args[0] == "") {
                        NebulaDebug.Log("dont spawn missingno");
                        return;
                    }
                    _entManager.SpawnEntity(e.Args[0]);
                } else {
                    devConsole.ExecManual("spawn");
                }
            }, NebulaHelper.GetConsoleCommandHelp("spawn"));

            devConsole.AddCommand("rm", delegate(object obj,
                VosSoft.Xna.GameConsole.CommandEventArgs e) {
                if (e.Args.Length > 0) {
                    for (var i = 0; i < e.Args.Length; i++) {
                        foreach (Entity ent in _entManager.ActiveEntities) {
                            if (ent.InstanceID == int.Parse(e.Args[i])) {
                                _entManager.MarkEntityForRemoval(ent.InstanceID);
                            }
                        }
                    }
                }
            }, NebulaHelper.GetConsoleCommandHelp("rm"));
        }
#endif
        #endregion
    }
}