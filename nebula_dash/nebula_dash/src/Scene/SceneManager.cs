#region Using Statements
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Keeps a list of scene objects, runs update loops (Update(), FixedUpdate(), Draw()) on all scenes
    /// </summary>
    public class SceneManager : INebulaSceneManagerService
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public SceneManager()
        {
            NebulaDebug.Assert(!_instantiated);
            _idCounter = 0;
            _scenes = new List<Scene>();
            _instantiated = true;

#if DEBUG
            AddConsoleCommands();
#endif
        }
        #endregion

        #region Fields
        private int _idCounter;
        private static bool _instantiated;

        // Scene stack
        private readonly List<Scene> _scenes;
        #endregion

        #region Update Loops
        /// <summary>
        /// Update loop
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// <param name="inputHandler">Detects input from HID</param>
        public void Update(GameTime gameTime, INebulaInputHandlerService inputHandler)
        {
            // Create working copy of _scenes
            var scenesToUpdate = new List<Scene>(_scenes);

            // Update all active Scenes
            while (scenesToUpdate.Count > 0) {
                Scene scn = scenesToUpdate[scenesToUpdate.Count - 1];
                scenesToUpdate.RemoveAt(scenesToUpdate.Count - 1);

                if (scn.RemoveMeFromList) {
                    _scenes.Remove(scn);
                } else {
                    scn.Update(gameTime, inputHandler);
                }
            }
        }

        /// <summary>
        /// FixedUpdate loop
        /// </summary>
        public void FixedUpdate()
        {
            foreach (Scene scn in _scenes) {
                scn.FixedUpdate();
            }
        }

        /// <summary>
        /// Draw loop
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// <param name="spriteBatch">Enables a group of sprites to be drawn using the same settings.</param>
        /// <param name="texContent">Map of all Texture2Ds to be drawn in this System</param>
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Dictionary<string, Texture2D> texContent)
        {
            foreach (Scene scn in _scenes) {
                if (scn.SceneState == SceneTransitionState.Hidden) {
                    continue;
                }
                scn.Draw(gameTime, spriteBatch, texContent);
            }
        }
        #endregion

        #region Interface Functions
        /// <summary>
        /// Add a scene to the update stack, and assign it a uniqueID
        /// </summary>
        /// <param name="scn">Scene to be added to updatable scenes</param>
        public void AddScene(Scene scn)
        {
            scn.InstanceID = _idCounter;
            _idCounter++;
            _scenes.Add(scn);
        }
        #endregion

        #region Private Functions
        private void ClearAllActiveScenes()
        {
            _scenes.Clear();
        }
        #endregion

        #region Debugging
#if DEBUG
        private void AddConsoleCommands()
        {
            NebulaHelper.RemoveConsoleCommands("SceneManager");
            var devConsole = GameServices.GetService<VosSoft.Xna.GameConsole.GameConsole>();

            devConsole.AddCommand("get_scn", delegate(object obj,
                VosSoft.Xna.GameConsole.CommandEventArgs e) {
                var nebulaDebug = GameServices.GetService<INebulaDebugService>();
                if (e.Args.Length > 0) {
                    int n;
                    bool result = int.TryParse(e.Args[0], out n);
                    if (result) {
                        List<Scene> l = _scenes.Where(scn => scn.InstanceID == n).ToList();
                        nebulaDebug.PrintScenes(l, true);
                    } else if (e.Args[0] == "properties" || e.Args[0] == "p") {
                        nebulaDebug.PrintScenes(_scenes, true);
                    } else {
                        NebulaDebug.Log("supply either a scn ID, or the argument `p`");
                    }
                } else {
                    nebulaDebug.PrintScenes(_scenes);
                }
            }, NebulaHelper.GetConsoleCommandHelp("get_scn"));

            devConsole.AddCommand("map", delegate(object obj,
                VosSoft.Xna.GameConsole.CommandEventArgs com) {
                AddScene(new MapScene(com.Args[0]));
                NebulaDebug.Log($"Map ID {com.Args[0]} spawned.");
            }, NebulaHelper.GetConsoleCommandHelp("map"));

            devConsole.AddCommand("rm_scn", delegate(object sender,
                VosSoft.Xna.GameConsole.CommandEventArgs e) {
                foreach (Scene scn in _scenes) {
                    if (scn.InstanceID == int.Parse(e.Args[0])) {
                        scn.TransitionPosition = 1;
                        scn.Exiting = true;
                    }
                }
            }, NebulaHelper.GetConsoleCommandHelp("rm_scn"));
        }
#endif
        #endregion
    }
}