﻿namespace nebula_dash
{
#if WINDOWS || XBOX
    internal static class Program
    {
        // The main entry point for the application.
        private static void Main(string[] args)
        {
            using (var game = new NebulaDash()) {
                game.Run();
            }
        }
    }
#endif
}