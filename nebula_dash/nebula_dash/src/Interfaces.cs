namespace nebula_dash
{
    /// <summary>
    /// Set of data that defines an Entity
    /// </summary>
    public interface INebulaComponent
    {
        /// <summary>
        /// Unique ID
        /// </summary>
        int InstanceID { get; set; }
    }

    /// <summary>
    /// Act on an entity's component data. Usually a gameplay system.
    /// </summary>
    public interface INebulaComponentSystem {}
}