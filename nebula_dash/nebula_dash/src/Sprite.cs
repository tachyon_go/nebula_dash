namespace nebula_dash
{
    /// <summary>
    /// Holds data required to draw Texture2D objects at runtime.
    /// </summary>
    public class Sprite
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public Sprite()
        {
            Name = "blank";
            Columns = 1;
            Rows = 1;
            FrameRange = 1;
            CurrentFrame = 0;
            SampleRate = 0;
            Accumulator = 0;
        }
        #endregion

        #region Copy Constructor
        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="bSprite">Base sprite to use for new sprite's initial property values</param>
        public Sprite(Sprite bSprite)
            : this()
        {
            Name = bSprite.Name;
            Columns = bSprite.Columns;
            Rows = bSprite.Rows;
            FrameRange = bSprite.FrameRange;
            SampleRate = bSprite.SampleRate;
            CurrentFrame = bSprite.CurrentFrame;
            Accumulator = bSprite.Accumulator;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Name corresponds to TextureContent map key
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Number of columns in sprite sheet (1 for non-animated images)
        /// </summary>
        public int Columns { get; set; }

        /// <summary>
        /// Number of rows in sprite sheet (1 for non-animated images)
        /// </summary>
        public int Rows { get; set; }

        /// <summary>
        /// Total number of individual frames in a sprite sheet (1 for non-animated images)
        /// </summary>
        public int FrameRange { get; set; }

        /// <summary>
        /// Speed at which <see cref="DrawSystem" /> will cycle through frames
        /// </summary>
        public int SampleRate { get; set; }

        /// <summary>
        /// Property used by <see cref="DrawSystem" /> for sprite animation
        /// </summary>
        public int CurrentFrame { get; set; }

        /// <summary>
        /// Property used by <see cref="DrawSystem" /> for sprite animation
        /// </summary>
        public int Accumulator { get; set; }
        #endregion
    }
}