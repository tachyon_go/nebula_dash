namespace nebula_dash
{
    /// <summary>
    /// Defines the Scene's current state of activity
    /// </summary>
    public enum SceneTransitionState
    {
        /// <summary>
        /// The scene is not yet active, but is transitioning into the Active state
        /// </summary>
        TransitionOn,

        /// <summary>
        /// The scene is active, and accepts input
        /// </summary>
        Active,

        /// <summary>
        /// The scene is transitioning away, and will be removed after <see cref="Scene.TransitionOffTime"/> expires
        /// </summary>
        TransitionOff,

        /// <summary>
        /// The scene is not in a transitionary state, but will not accept input or be drawn
        /// </summary>
        Hidden
    }

    /// <summary>
    /// Mouse button enumeration, identifies a particular button on a mouse
    /// </summary>
    public enum MouseButtons
    {
        /// <summary>
        /// Right mouse button (Mouse 1)
        /// </summary>
        RightClick,
        
        /// <summary>
        /// Left mouse button (Mouse 2)
        /// </summary>
        LeftClick,
        
        /// <summary>
        /// Middle mouse button (Mouse 3)
        /// </summary>
        MiddleClick,

        /// <summary>
        /// Extra button 1 (Mouse 4)
        /// </summary>
        XButton1,

        /// <summary>
        /// Extra button 2 (Mouse 5)
        /// </summary>
        XButton2
    }
}