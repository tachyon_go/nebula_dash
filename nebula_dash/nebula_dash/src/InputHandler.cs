#region Using Statements
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using VosSoft.Xna.GameConsole;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Accepts input from a HID. Should normally be used through the INebulaInputHandlerService interface.
    /// </summary>
    public class InputHandler : INebulaInputHandlerService
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public InputHandler()
        {
            NebulaDebug.Assert(!_instantiated);
            KbState = new KeyboardState();
            OldKbState = new KeyboardState();
            MState = new MouseState();
            OldMouseState = new MouseState();
            _instantiated = true;
        }
        #endregion

        #region Fields
        private static bool _instantiated;
        private KeyboardState _oldK;
        private int _oldMScroll;
        #endregion

        #region Properties
        /// <summary>
        /// The current KeyboardState
        /// </summary>
        public KeyboardState KbState { get; private set; }
        
        /// <summary>
        /// The keyboard state of the last Update
        /// </summary>
        public KeyboardState OldKbState { get; private set; }
        
        /// <summary>
        /// The current MouseState
        /// </summary>
        public MouseState MState { get; private set; }

        /// <summary>
        /// The mouse state of the last Update
        /// </summary>
        public MouseState OldMouseState { get; private set; }
        #endregion

        #region Update Loops
        /// <summary>
        /// Update loop
        /// </summary>
        public void Update()
        {
            // Mouse
            MState = Mouse.GetState();
            int mwheel = MState.ScrollWheelValue > _oldMScroll
                ? -1 : (MState.ScrollWheelValue < _oldMScroll ? 1 : 0);
            _oldMScroll = MState.ScrollWheelValue;

            // KB
            var devConsole = GameServices.GetService<GameConsole>();

            KeyboardState k = Keyboard.GetState();

            // Toggle devconsole input:
            if (_oldK.IsKeyDown(Keys.F2) && k.IsKeyUp(Keys.F2)) {
                devConsole.InputEnabled = !devConsole.InputEnabled;
            }

            _oldK = k;

            // don't accept input anymore if input is going to devconsole
            if (devConsole.IsOpen && devConsole.InputEnabled) {
                return;
            }

            KbState = Keyboard.GetState();

            // open devconsole
            if (PressedOnceKb(Keys.F1)) {
                devConsole.Open(Keys.F1);
                devConsole.InputEnabled = true;
            }
        }
        #endregion

        #region Interface Functions
        /// <summary>
        /// Returns true once per button press
        /// </summary>
        /// <param name="key">Key to check for button press</param>
        /// <returns></returns>
        public bool PressedOnceKb(Keys key)
        {
            bool isPressed;

            if (OldKbState.IsKeyUp(key) && KbState.IsKeyDown(key)) {
                isPressed = true;
            } else {
                isPressed = false;
            }

            return isPressed;
        }

        /// <summary>
        /// Returns true while a mouse button is held down
        /// </summary>
        /// <param name="mouseButton">Mouse button to check for press</param>
        /// <returns></returns>
        public bool PressedMouse(MouseButtons mouseButton)
        {
            var isPressed = false;

            switch (mouseButton) {
                case MouseButtons.LeftClick:
                    isPressed = OldMouseState.LeftButton == ButtonState.Pressed &&
                                MState.LeftButton == ButtonState.Pressed ? true : false;
                    break;
                case MouseButtons.RightClick:
                    isPressed = OldMouseState.RightButton == ButtonState.Pressed &&
                                MState.RightButton == ButtonState.Pressed ? true : false;
                    break;
                case MouseButtons.MiddleClick:
                    isPressed = OldMouseState.MiddleButton == ButtonState.Pressed &&
                                MState.MiddleButton == ButtonState.Pressed ? true : false;
                    break;
                case MouseButtons.XButton1:
                    isPressed = OldMouseState.XButton1 == ButtonState.Pressed &&
                                MState.XButton1 == ButtonState.Pressed ? true : false;
                    break;
                case MouseButtons.XButton2:
                    isPressed = OldMouseState.XButton2 == ButtonState.Pressed &&
                                MState.XButton2 == ButtonState.Pressed ? true : false;
                    break;
                default:
                    NebulaDebug.ExceptionMessage(
                        "You're trying to use a MouseButton that doesn't exist.\n\nValue: " +
                        mouseButton +
                        "\nValid values: LeftButton, RightButton, MiddleButton, XButton1, XButton2");
                    Debugger.Break();
                    break;
            }

            return isPressed;
        }

        /// <summary>
        /// Returns true once per mouse click
        /// </summary>
        /// <param name="mouseButton">Mouse button to check for press</param>
        /// <returns></returns>
        public bool PressedOnceMouse(MouseButtons mouseButton)
        {
            var isPressed = false;

            switch (mouseButton) {
                case MouseButtons.LeftClick:
                    isPressed = OldMouseState.LeftButton == ButtonState.Released &&
                                MState.LeftButton == ButtonState.Pressed ? true : false;
                    break;
                case MouseButtons.RightClick:
                    isPressed = OldMouseState.RightButton == ButtonState.Released &&
                                MState.RightButton == ButtonState.Pressed ? true : false;
                    break;
                case MouseButtons.MiddleClick:
                    isPressed = OldMouseState.MiddleButton == ButtonState.Released &&
                                MState.MiddleButton == ButtonState.Pressed ? true : false;
                    break;
                case MouseButtons.XButton1:
                    isPressed = OldMouseState.XButton1 == ButtonState.Released &&
                                MState.XButton1 == ButtonState.Pressed ? true : false;
                    break;
                case MouseButtons.XButton2:
                    isPressed = OldMouseState.XButton2 == ButtonState.Released &&
                                MState.XButton2 == ButtonState.Pressed ? true : false;
                    break;
                default:
                    NebulaDebug.ExceptionMessage(
                        "You're trying to use a MouseButton that doesn't exist.\n\nValue: " +
                        mouseButton +
                        "\nValid values: LeftButton, RightButton, MiddleButton, XButton1, XButton2");
                    break;
            }

            return isPressed;
        }

        /// <summary>
        /// Returns the current mouse position. Supply a Camera argument to retrieve the world-coordinate position
        /// </summary>
        /// <param name="cam">If provided, return world coordinates instead of screen coordinates</param>
        /// <returns></returns>
        public Vector2 GetMousePosition(Camera cam = null)
        {
            Vector2 mousePos;

            if (cam != null) {
                mousePos = Vector2.Transform(new Vector2(MState.X, MState.Y),
                    Matrix.Invert(cam.GetViewMatrix()));
            } else {
                mousePos = new Vector2(MState.X, MState.Y);
            }

            return mousePos;
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Used in the main game loop to reset state after the World uses it
        /// </summary>
        public void ResetState()
        {
            OldKbState = KbState;
            OldMouseState = MState;
        }
        #endregion
    }
}