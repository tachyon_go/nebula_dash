#region Using Statements
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using VosSoft.Xna.GameConsole;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// HYPERNOVA EXPLOSION!?
    /// </summary>
    public class NebulaDash : Game
    {
        #region Default Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public NebulaDash()
        {
            _graphics = new GraphicsDeviceManager(this);
            _devConsole = new GameConsole(this);
        }
        #endregion

        #region Fields
        private readonly GraphicsDeviceManager _graphics;
        private readonly GameConsole _devConsole;
        private SpriteBatch _spriteBatch;
        private InputHandler _inputHandler;
        private SceneManager _scnManager;
        private Dictionary<string, Texture2D> _texContent;

        // Fields for Fixed timestep loop
        private double _accumulator;
        private const double DELTA_TIME = 7;
        #endregion

        #region Initialization
        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            IsFixedTimeStep = false;
            IsMouseVisible = true;

            var ini = new INIParser("config.ini");

            _graphics.PreferredBackBufferWidth = ini.ReadInt("ScreenWidth", "Graphics", 960);
            _graphics.PreferredBackBufferHeight = ini.ReadInt("ScreenHeight", "Graphics", 540);
            _graphics.PreferMultiSampling = ini.ReadBool("PreferMultiSampling", "Graphics", true);
            _graphics.SynchronizeWithVerticalRetrace = ini.ReadBool(
                "SynchronizedWithVerticalRetrace", "Graphics");
            _graphics.ApplyChanges();
            GameServices.AddService<GraphicsDeviceManager>(_graphics);
            GameServices.AddService(_devConsole);
            NebulaHelper.InitializeDevConsole();
            Components.Add(new FrameRateCounter(this));
            _inputHandler = new InputHandler();
            GameServices.AddService<INebulaInputHandlerService>(_inputHandler);
            _scnManager = new SceneManager();
            GameServices.AddService<INebulaSceneManagerService>(_scnManager);
            GameServices.AddService<INebulaDebugService>(new NebulaDebug());

            _scnManager.AddScene(new MapScene("training"));

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            Content.RootDirectory = "Content";
            _texContent = Content.LoadListContent<Texture2D>("tex");
            GameServices.AddService<Dictionary<string, Texture2D>>(_texContent);
            _spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            Content.Unload();
        }
        #endregion

        #region Update loops
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            _inputHandler.Update();
            _scnManager.Update(gameTime, _inputHandler);
            _inputHandler.ResetState();

            // Below while loop is the "FixedUpdate" logic:
            // http://gafferongames.com/game-physics/fix-your-timestep/
            _accumulator += gameTime.ElapsedGameTime.TotalMilliseconds;
            while (_accumulator >= DELTA_TIME) {
                _scnManager.FixedUpdate();
                _accumulator -= DELTA_TIME;
            }

            // Allows the game to exit
            if (Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                Exit();
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <remarks>
        /// More about calling update/draw here:
        /// http://blogs.msdn.com/b/shawnhar/archive/2007/11/23/game-timing-in-xna-game-studio-2-0.aspx
        /// </remarks>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            _scnManager.Draw(gameTime, _spriteBatch, _texContent);
            base.Draw(gameTime);
        }
        #endregion
    }
}