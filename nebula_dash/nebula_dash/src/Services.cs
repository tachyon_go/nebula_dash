#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Use this to register, retrieve, or remove game services
    /// </summary>
    /// <remarks>
    /// http://roy-t.nl/index.php/2010/08/25/xna-accessing-contentmanager-and-graphicsdevice-anywhere-anytime-the-gameservicecontainer/
    /// </remarks>
    public static class GameServices
    {
        private static GameServiceContainer _container;

        private static GameServiceContainer Instance {
            get { return _container ?? (_container = new GameServiceContainer()); }
        }

        /// <summary>
        /// Supply a type to receive a service of that type
        /// </summary>
        /// <typeparam name="T">Type of the desired service</typeparam>
        /// <returns></returns>
        public static T GetService<T>()
        {
            return (T)Instance.GetService(typeof (T));
        }

        /// <summary>
        /// Pass a reference to register it as a service for the specified type
        /// </summary>
        /// <typeparam name="T">Type of service to register reference as</typeparam>
        /// <param name="service">Reference to be added as a service</param>
        public static void AddService<T>(T service)
        {
            Instance.AddService(typeof (T), service);
        }

        /// <summary>
        /// Unregister a service of the specificed type
        /// </summary>
        /// <typeparam name="T">Service type to unregister</typeparam>
        public static void RemoveService<T>()
        {
            Instance.RemoveService(typeof (T));
        }
    }

    /// <summary>
    /// Service allowing you to interact with the main stack of scenes.
    /// </summary>
    public interface INebulaSceneManagerService
    {
        /// <summary>
        /// Add a scene to the update stack, and assign it a uniqueID
        /// </summary>
        /// <param name="scn">Scene to be added to updatable scenes</param>
        void AddScene(Scene scn);
    }

    /// <summary>
    /// Provides information regarding HID state.
    /// </summary>
    public interface INebulaInputHandlerService
    {
        /// <summary>
        /// The current KeyboardState
        /// </summary>
        Microsoft.Xna.Framework.Input.KeyboardState KbState { get; }

        /// <summary>
        /// The current MouseState
        /// </summary>
        Microsoft.Xna.Framework.Input.MouseState MState { get; }

        /// <summary>
        /// Returns true once per button press
        /// </summary>
        /// <param name="key">Key to check for button press</param>
        /// <returns></returns>
        bool PressedOnceKb(Microsoft.Xna.Framework.Input.Keys key);

        /// <summary>
        /// Returns true while a mouse button is held down
        /// </summary>
        /// <param name="mouseButton">Mouse button to check for press</param>
        /// <returns></returns>
        bool PressedMouse(MouseButtons mouseButton);

        /// <summary>
        /// Returns true once per mouse click
        /// </summary>
        /// <param name="mouseButton">Mouse button to check for press</param>
        /// <returns></returns>
        bool PressedOnceMouse(MouseButtons mouseButton);

        /// <summary>
        /// Returns the current mouse position. Supply a Camera argument to retrieve the world-coordinate position
        /// </summary>
        /// <param name="cam">If provided, return world coordinates instead of screen coordinates</param>
        /// <returns></returns>
        Vector2 GetMousePosition(Camera cam = null);
    }

    /// <summary>
    /// Exposes tools that are useful for debugging and testing
    /// </summary>
    public interface INebulaDebugService
    {
#if DEBUG
        /// <summary>
        /// Display information about an array of active scenes
        /// </summary>
        /// <param name="activeScenes">Scenes to print information about</param>
        /// <param name="withProperties">Display individual property values?</param>
        void PrintScenes(System.Collections.Generic.List<Scene> activeScenes,
            bool withProperties = false);

        /// <summary>
        /// Display information about an array of active entities
        /// </summary>
        /// <param name="entities">Entities to print information about</param>
        /// <param name="withComponents">Include their components instance IDs?</param>
        /// <param name="withProperties">Include the properties of their components?</param>
        /// <param name="osd">Not used right now, coming soon</param>
        void PrintEntities(System.Collections.Generic.List<Entity> entities,
            bool withComponents = false, bool withProperties = false, bool osd = false);

        /// <summary>
        /// Print information about a specific component in an array of entities
        /// </summary>
        /// <param name="entities">Entities to print information about</param>
        /// <param name="componentID">Instance ID of specific component</param>
        void PrintEntities(System.Collections.Generic.List<Entity> entities, int componentID);

        /// <summary>
        /// Returns a list of all an entity's current components
        /// </summary>
        /// <param name="ent">Entity to grab components of</param>
        /// <returns></returns>
        System.Collections.Generic.IEnumerable<INebulaComponent> CheckEntityComponents(Entity ent);
#endif
    }
}