#pragma warning disable 1591
#region Using Statements
using System;
using Microsoft.Xna.Framework;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Component that displays a game's current framerate
    /// </summary>
    public class FrameRateCounter : DrawableGameComponent
    {
        //public Vector2 Position { get; set; }
        //public Vector2 Position2 { get; set; }
        //public Color Color { get; set; }
        //public Color Color2 { get; set; }

        //private ContentManager content;
        //private SpriteBatch spriteBatch;
        //private SpriteFont spriteFont;

        private int _frameRate;
        private int _frameCounter;
        private TimeSpan _elapsedTime = TimeSpan.Zero;

        public FrameRateCounter(Game game)
            : base(game) {}

        public FrameRateCounter(Game game, Vector2 position, Color color, Color color2)
            : base(game)
        {
            //content = game.Content;
            //Position = position;
            //Position2 = new Vector2(Position.X + 1, Position.Y + 1);
            //Color = color;
            //Color2 = color2;
        }

        //protected override void LoadContent()
        //{
        //    spriteBatch = new SpriteBatch(GraphicsDevice);
        //    spriteFont = content.Load<SpriteFont>("Fonts/fpsCounter");
        //}

        //protected override void UnloadContent()
        //{
        //    content.Unload();
        //}

        public override void Update(GameTime gameTime)
        {
            _elapsedTime += gameTime.ElapsedGameTime;

            if (_elapsedTime > TimeSpan.FromSeconds(1)) {
                _elapsedTime -= TimeSpan.FromSeconds(1);
                _frameRate = _frameCounter;
                _frameCounter = 0;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _frameCounter++;
            Game.Window.Title = "NEBULA DASH || fps: " + _frameRate;

            //string text = "fps: " + frameRate;

            //spriteBatch.Begin();
            //spriteBatch.DrawString(spriteFont, text, Position2, Color2);
            //spriteBatch.DrawString(spriteFont, text, Position, Color);
            //spriteBatch.End();
        }
    }
}