#region Using Statements
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Static helper class for utility functions
    /// </summary>
    public static class NebulaHelper
    {
        #region Static constructor
        static NebulaHelper()
        {
            InitialConsoleCommands = new Dictionary<string, List<string[]>>();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Contains the default DevConsole commands available when the game first launches.
        /// </summary>
        public static Dictionary<string, List<string[]>> InitialConsoleCommands { get; }
        #endregion

        #region Public Functions
        /// <summary>
        /// <see cref="Microsoft.Xna.Framework.MathHelper.Clamp(float, float, float)" /> only does floats,
        /// this function is for integers
        /// </summary>
        /// <param name="value">The value to clamp.</param>
        /// <param name="min">The minimum value. If value is less than min, min will be returned.</param>
        /// <param name="max">The maximum value. If value is greater than max, max will be returned.</param>
        /// <returns></returns>
        public static int ClampInt(int value, int min, int max)
        {
            return value < min ? min : value > max ? max : value;
        }

        /// <summary>
        /// Load all content in a directory into a dictionary.
        /// Returned dictionary uses string keys that correspond to a content item's filename (without extension).
        /// </summary>
        /// <typeparam name="T">Type to load all content from directory as</typeparam>
        /// <param name="contentManager">Reference to game's content manager</param>
        /// <param name="contentDirectory">Directory to load all content of</param>
        /// <returns>Dictionary containing references to all content from specified directory</returns>
        public static Dictionary<string, T> LoadListContent<T>(this ContentManager contentManager,
            string contentDirectory)
        {
            var dir = new DirectoryInfo(contentManager.RootDirectory + "/" + contentDirectory);

            if (!dir.Exists) {
                throw new DirectoryNotFoundException();
            }
            var result = new Dictionary<string, T>();

            FileInfo[] files = dir.GetFiles("*.*");

            foreach (FileInfo file in files) {
                string key = Path.GetFileNameWithoutExtension(file.Name);
                result[key] = contentManager.Load<T>(contentDirectory + "/" + key);
            }
            return result;
        }

        /// <summary>
        /// Initializes console values, and adds all console commands (some skeletal, as they require access to
        /// initially unavailable class members).
        /// </summary>
        public static void InitializeDevConsole()
        {
            var devConsole = GameServices.GetService<VosSoft.Xna.GameConsole.GameConsole>();

            // Initialize values
            devConsole.LogDebugMessages = true;
            devConsole.OpeningAnimation = VosSoft.Xna.GameConsole.GameConsoleAnimation.None;
            devConsole.ClosingAnimation = VosSoft.Xna.GameConsole.GameConsoleAnimation.None;
            devConsole.MaxLogEntries = 10000;
            devConsole.Lines = 15;
            devConsole.SetLogLevelColor(255, Color.Pink);
            devConsole.SetLogLevelColor(1, Color.MediumVioletRed);

            var ini = new INIParser("config.ini");

            // Add commands
            devConsole.AddCommand("tog_vsync", delegate {
                var graphics = GameServices.GetService<GraphicsDeviceManager>();
                graphics.SynchronizeWithVerticalRetrace = !ini.ReadBool("SynchronizedWithVerticalRetrace", "Graphics");
                ini.WriteBool("SynchronizedWithVerticalRetrace", !ini.ReadBool("SynchronizedWithVerticalRetrace",
                    "Graphics"), "Graphics");
                graphics.ApplyChanges();
                NebulaDebug.Log(
                    string.Format("vsync on: " + graphics.SynchronizeWithVerticalRetrace));
            }, "Toggle synchronization with vertical retrace");

            // These commands aren't available yet, add skeletal version for the help output
            // TODO: THIS IS SUPER DUMB I NEED TO REVISE GAMECONSOLE TO LOAD ALL THIS FROM DATA FILES
            InitialConsoleCommands.Add("Scene", new List<string[]>() {
                new string[] {
                    "mc",
                    "Returns the mouse cursor's current position in world-coordinates"
                }
            });

#if DEBUG
            InitialConsoleCommands.Add("SceneManager", new List<string[]>() {
                new string[] {
                    "get_scn",
                    "Print scene information",
                    "get_scn [id] [p]"
                },
                new string[] {
                    "map",
                    "Spawn a mapscene, based on ID from map dict",
                    "map <mapID>"
                },
                new string[] {
                    "rm_scn",
                    "Remove a scene",
                    "Sets scn.RemoveMeFromList to true",
                    "rm_scn <scn.instanceID>"
                }
            });

            InitialConsoleCommands.Add("MapScene", new List<string[]>() {
                new string[] {
                    "get",
                    "Print entity data",
                    "get [component|c] [cp]"
                },
                new string[] {
                    "set",
                    "Change property values on an entity's components",
                    "set <ent.InstanceID> <component.InstanceID> <propertyName> <newValue>"
                },
                new string[] {
                    "spawn",
                    "Spawn an Entity from the master entity template",
                    "Entity ID can be found in ent_data.neb",
                    "rm <ent.InstanceID>"
                },
                new string[] {
                    "rm",
                    "Remove an entity",
                    "Sets ent.RemoveMeFromList to true",
                    "rm <ent.InstanceID>"
                }
            });
#endif

            foreach (KeyValuePair<string, List<string[]>> kvp in InitialConsoleCommands) {
                foreach (string[] sa in kvp.Value) {
                    devConsole.AddCommand(sa[0],
                        delegate {
                            devConsole.Log(
                                $"Unavailable command. Instance of {kvp.Key} class is required before usage.");
                        }, sa[1]);
                }
            }
        }

        /// <summary>
        /// Supply console command, returns the help documentation
        /// </summary>
        /// <param name="command">Command you'd like help documentation for</param>
        public static string[] GetConsoleCommandHelp(string command)
        {
            string[] s = {"No help documentation found"};

            foreach (KeyValuePair<string, List<string[]>> kvp in InitialConsoleCommands) {
                foreach (string[] sa in kvp.Value) {
                    if (sa[0] == command && sa.Length > 1) {
                        s = new string[sa.Length - 1];
                        for (var i = 0; i < s.Length; i++) {
                            s[i] = sa[i + 1];
                        }
                    }
                }
            }

            return s;
        }

        /// <summary>
        /// Remove console commands previously added in this runtime.
        /// Should be called before calling GameConsole.AddCommand() in any class that is expected to
        /// be instantiated multiple times during runtime.
        /// </summary>
        /// <param name="className">Object type to remove commands of</param>
        public static void RemoveConsoleCommands(string className)
        {
            var devConsole = GameServices.GetService<VosSoft.Xna.GameConsole.GameConsole>();

            if (InitialConsoleCommands.ContainsKey(className)) {
                for (var i = 0; i < InitialConsoleCommands[className].Count; i++) {
                    devConsole.RemoveCommand(InitialConsoleCommands[className][i][0]);
                }
            } else {
                NebulaDebug.ExceptionMessage(className + " does not exist as a key in InitialConsoleCommands");
                System.Diagnostics.Debugger.Break();
            }
        }
        #endregion
    }
}