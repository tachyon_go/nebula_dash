#region Using Statements
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using VosSoft.Xna.GameConsole;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// General debug class. Registered as a service using the INebulaDebugService interface, 
    /// so use that instead.
    /// </summary>
    public class NebulaDebug : INebulaDebugService
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public NebulaDebug()
        {
            Assert(!_instantiated);
            _instantiated = true;
        }
        #endregion

        #region Fields
        private static bool _instantiated;
        #endregion

        #region Public Functions
        /// <summary>
        /// ExceptionMessage displays in a messagebox and in console. Prints Environment.StackTrace after message.
        /// </summary>
        /// <param name="s">String to display for the message</param>
        public static void ExceptionMessage(string s = "")
        {
            Console.WriteLine(s + "\n\nStack trace:\n" + Environment.StackTrace);
            MessageBox(new IntPtr(0), s + "\n\nStack trace:\n" + Environment.StackTrace,
                "NEBULA DASH EXCEPTION", 0);
        }

        /// <summary>
        /// Aggressive assertion check that calls Debugger.Break on failure
        /// </summary>
        /// <param name="c">Value to assert</param>
        public static void Assert(bool c)
        {
            if (!c) {
                ExceptionMessage("Assert failed.");
                Debugger.Break();
            }
        }

        /// <summary>
        /// Log a string to the developer console and cmd
        /// </summary>
        /// <param name="str">String to log to developer console</param>
        public static void Log(string str)
        {
            var devConsole = GameServices.GetService<GameConsole>();
            devConsole.Log(str);
            Console.WriteLine(str);
        }
        #endregion

        #region Private Functions
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern uint MessageBox(IntPtr hWnd, string text, string caption, uint type);
        #endregion

        #region Debugging
#if DEBUG
        #region Interface Functions
        /// <summary>
        /// Display information about an array of active scenes
        /// </summary>
        /// <param name="activeScenes">Scenes to print information about</param>
        /// <param name="withProperties">Display individual property values?</param>
        public void PrintScenes(List<Scene> activeScenes, bool withProperties = false)
        {
            Log("\n----- ACTIVE SCENES -----\n");

            foreach (Scene scn in activeScenes) {
                Log("* Scene Name: " + scn.Name + " // Instance ID: " + scn.InstanceID);
                if (withProperties) {
                    foreach (PropertyInfo p in
                        scn.GetType().GetProperties().Where(
                            p => !p.GetGetMethod().GetParameters().Any())) {
                        Log("   -" + p.Name + ": " + p.GetValue(scn, null));
                    }
                }
            }
        }

        /// <summary>
        /// Display information about an array of active entities
        /// </summary>
        /// <param name="entities">Entities to print information about</param>
        /// <param name="withComponents">Include their components instance IDs?</param>
        /// <param name="withProperties">Include the properties of their components?</param>
        /// <param name="osd">Not used right now, coming soon</param>
        public void PrintEntities(List<Entity> entities, bool withComponents = false,
            bool withProperties = false, bool osd = false)
        {
            if (withComponents) {
                Log("\n----- ENTITIES & THEIR COMPONENTS -----\n");

                foreach (Entity ent in entities) {
                    Log("* Entity Name: " + ent.Name + " // Instance ID: " + ent.InstanceID);

                    IEnumerable<INebulaComponent> componentList = CheckEntityComponents(ent);

                    foreach (INebulaComponent component in componentList) {
                        Log("   -" + component + " // Instance ID: " + component.InstanceID);

                        if (withProperties) {
                            foreach (PropertyInfo p in
                                component.GetType().GetProperties().Where(
                                    p => !p.GetGetMethod().GetParameters().Any())) {
                                Log("     -" + p.Name + ": " + p.GetValue(component, null));
                            }
                        }
                    }
                }
            } else {
                Log("\n\nENTITIES\n");

                foreach (Entity ent in entities) {
                    Log(ent.InstanceID + " " + ent.Name + " " + ent.PositionComponent.Position);
                }
            }
        }

        /// <summary>
        /// Print information about a specific component in an array of entities
        /// </summary>
        /// <param name="entities">Entities to print information about</param>
        /// <param name="componentID">Instance ID of specific component</param>
        public void PrintEntities(List<Entity> entities, int componentID)
        {
            Log("\n----- ENTITIES & THEIR COMPONENTS -----\n");

            foreach (Entity ent in entities) {
                Log("* Entity Name: " + ent.Name + " // Instance ID: " + ent.InstanceID);

                IEnumerable<INebulaComponent> componentList = CheckEntityComponents(ent);

                foreach (INebulaComponent component in componentList) {
                    if (component.InstanceID == componentID) {
                        Log("   -" + component + " // Instance ID: " + component.InstanceID);
                        foreach (PropertyInfo p in
                            component.GetType().GetProperties().Where(
                                p => !p.GetGetMethod().GetParameters().Any())) {
                            Log("     -" + p.Name + ": " + p.GetValue(component, null));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns a list of all an entity's current components
        /// </summary>
        /// <param name="ent">Entity to grab components of</param>
        /// <returns></returns>
        public IEnumerable<INebulaComponent> CheckEntityComponents(Entity ent)
        {
            var componentList = new List<INebulaComponent>();

            if (ent.PositionComponent != null) {
                componentList.Add(ent.PositionComponent);
            }
            if (ent.DrawComponent != null) {
                componentList.Add(ent.DrawComponent);
            }
            return componentList;
        }
        #endregion
#endif
        #endregion
    }
}