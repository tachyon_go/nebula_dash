namespace nebula_dash
{
    /// <summary>
    /// Creates components and assigns them IDs, usually called through an <see cref="EntityManager" /> object
    /// </summary>
    public class ComponentFactory
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public ComponentFactory()
        {
            _idCounter = 0;
        }
        #endregion

        #region Fields
        private static int _idCounter;
        #endregion

        #region Public Functions
        /// <summary>
        /// Takes an original entity and clone entity, then sets the clone up with new
        /// components identical to the original's
        /// </summary>
        /// <param name="baseEntity"><see cref="Entity" /> to copy components from</param>
        /// <param name="newEntity"><see cref="Entity" /> to copy components to</param>
        public void CopyComponents(Entity baseEntity, Entity newEntity)
        {
            if (baseEntity.PositionComponent != null) {
                newEntity.PositionComponent = new PositionEntityComponent(baseEntity.PositionComponent);
                AssignComponentID(newEntity.PositionComponent);
            }
            if (baseEntity.DrawComponent != null) {
                newEntity.DrawComponent = new DrawEntityComponent(baseEntity.DrawComponent);
                AssignComponentID(newEntity.DrawComponent);
            }
        }

        /// <summary>
        /// Use this function to instantiate component objects, it'll setup some basics for you
        /// </summary>
        /// <param name="component">Component to be created</param>
        /// <returns>Component with ID</returns>
        public INebulaComponent CreateComponent(INebulaComponent component)
        {
            AssignComponentID(component);
            return component;
        }
        #endregion

        #region Private Functions
        // Give components a unique ID
        // We don't expose this directly atm, just in case we ever want to expand on the component spawning process
        private static void AssignComponentID(INebulaComponent component)
        {
            component.InstanceID = _idCounter;
            _idCounter++;
        }
        #endregion
    }
}