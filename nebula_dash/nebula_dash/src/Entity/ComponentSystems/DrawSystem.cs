#region Using Statements
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Run the Update, FixedUpdate, and Draw functions to draw 2D spritesheet based entities on screen
    /// </summary>
    public class DrawSystem : INebulaComponentSystem
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawSystem()
        {
            _parallaxLayersArray = new List<Entity>[0];
        }
        #endregion

        #region Fields
        private List<Entity>[] _parallaxLayersArray;
        private const int FRAMESTEP_DELTA = 100;
        #endregion

        #region Update Loops
        /// <summary>
        /// Update loop
        /// </summary>
        /// <param name="entities">Entities to be drawn</param>
        /// <param name="paralayerCount">Total number of elements in the parent Scene's ParallaxLayers list</param>
        public void Update(List<Entity> entities, int paralayerCount)
        {
            SetParallaxLayerArrayLength(paralayerCount);

            foreach (Entity ent in entities) {
                AddEntitiesToParallaxLayers(ent);
            }
        }

        /// <summary>
        /// FixedUpdate loop
        /// </summary>
        /// <param name="entities">Entities to be drawn</param>
        public void FixedUpdate(List<Entity> entities)
        {
            foreach (Entity ent in entities) {
                FrameStep(ent);

                ent.DrawComponent.TintColor = new Color(255, 255, 255, ent.DrawComponent.TintColor.A);
            }
        }

        /// <summary>
        /// Draw loop
        /// </summary>
        /// <param name="spriteBatch">Enables a group of sprites to be drawn using the same settings.</param>
        /// <param name="texContent">Map of all Texture2Ds to be drawn in this System</param>
        /// <param name="sceneCamera">Camera from the scene</param>
        /// <param name="paralayers">ParallaxLayer values from the scene</param>
        public void Draw(SpriteBatch spriteBatch, Dictionary<string, Texture2D> texContent,
            Camera sceneCamera, List<Vector2> paralayers)
        {
            if (_parallaxLayersArray != null) {
                // Start a spriteBatch for each ParallaxLayer
                for (int i = _parallaxLayersArray.Length - 1; i > -1; i--) {
                    // Begin spritebatch for whole Paralayer
                    spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend,
                        SamplerState.PointClamp, null, null, null,
                        sceneCamera.GetViewMatrix(paralayers[i].X, paralayers[i].Y));
                    foreach (Entity ent in _parallaxLayersArray[i]) {
                        Texture2D tex;

                        try {
                            tex = texContent[ent.DrawComponent.CurrentSprite.Name];
                        } catch (Exception) {
                            NebulaDebug.ExceptionMessage(
                                "Texture name doesn't exist in our texture map\n" + "Entity name: " +
                                ent.Name + " // Texture name: " +
                                ent.DrawComponent.CurrentSprite.Name);
                            Debugger.Break();
                            throw;
                        }

                        ent.DrawComponent.CurrentSprite = ent.DrawComponent.NextSprite;

                        Rectangle sourceRectangle;
                        Rectangle destinationRectangle;

                        // Try to setup the data for drawing the entity with animation, based on whatever FrameStep() has set
                        // Mostly lifted from rbwhitaker: http://rbwhitaker.wikidot.com/texture-atlases-2
                        try {
                            int width = tex.Width / ent.DrawComponent.CurrentSprite.Columns;
                            int height = tex.Height / ent.DrawComponent.CurrentSprite.Rows;
                            var row =
                                (int)
                                    (ent.DrawComponent.CurrentSprite.CurrentFrame /
                                     (float)ent.DrawComponent.CurrentSprite.Columns);
                            int column = ent.DrawComponent.CurrentSprite.CurrentFrame %
                                         ent.DrawComponent.CurrentSprite.Columns;

                            sourceRectangle = new Rectangle(width * column, height * row, width,
                                height);
                            destinationRectangle =
                                new Rectangle((int)ent.PositionComponent.Position.X,
                                    (int)ent.PositionComponent.Position.Y,
                                    ent.DrawComponent.Scale.X * width,
                                    ent.DrawComponent.Scale.Y * height);

                            ent.DrawComponent.Origin = new Vector2(width * 0.5f, height * 0.5f);
                        } catch (Exception) {
                            NebulaDebug.ExceptionMessage(
                                "Entity can't be drawn, did you forget to assign values to the " +
                                "DrawComponent fields?\nEntity Info:\n" + ent.Name + "(ID:" +
                                ent.InstanceID + ")\nSprite == " +
                                ent.DrawComponent.CurrentSprite.Name + "\nColumns == " +
                                ent.DrawComponent.CurrentSprite.Columns + " // Rows == " +
                                ent.DrawComponent.CurrentSprite.Rows + " // TotalFrames == " +
                                ent.DrawComponent.CurrentSprite.FrameRange);
                            Debugger.Break();
                            throw;
                        }

                        spriteBatch.Draw(tex, destinationRectangle, sourceRectangle,
                            ent.DrawComponent.TintColor, ent.DrawComponent.Rotation,
                            ent.DrawComponent.Origin, ent.DrawComponent.Flip,
                            ent.DrawComponent.Layer);
                    }
                    spriteBatch.End();
                }
            }
        }
        #endregion

        #region Private Functions
        // FrameStep(Entity) runs at an arbitrarily defined "sample rate" to specify the speed of an animation.
        // The logic is very similar to the main FixedUpdate timestep.
        // Q: Why do you roll your own delta instead of using gameTime?
        // A: CheatEngine "Speed Hack" checkbox
        private static void FrameStep(Entity ent)
        {
            ent.DrawComponent.CurrentSprite.Accumulator +=
                ent.DrawComponent.CurrentSprite.SampleRate;

            // Step through frames based on SampleRate
            while (ent.DrawComponent.CurrentSprite.Accumulator >= FRAMESTEP_DELTA) {
                ent.DrawComponent.CurrentSprite.CurrentFrame++;

                // Clamp CurrentFrame to TotalFrames - This should never be needed outside of test code, but if you update 
                // TotalFrames outside of FixedUpdate, it can start increasing dramatically, and CurrentFrame will
                // never catch up to reset. This "fix" prevents sprites from ever outright disappearing, 
                // instead causing them to flicker.
                ent.DrawComponent.CurrentSprite.CurrentFrame =
                    NebulaHelper.ClampInt(ent.DrawComponent.CurrentSprite.CurrentFrame, 0,
                        ent.DrawComponent.CurrentSprite.FrameRange);

                if (ent.DrawComponent.CurrentSprite.CurrentFrame ==
                    ent.DrawComponent.CurrentSprite.FrameRange) {
                    ent.DrawComponent.CurrentSprite.CurrentFrame = 0;
                }

                ent.DrawComponent.CurrentSprite.Accumulator -= FRAMESTEP_DELTA;
            }
        }

        // Initialize an array of "ParallaxLayers", which themselves are arrays holding Entities
        private void SetParallaxLayerArrayLength(int paralayerCount)
        {
            _parallaxLayersArray = new List<Entity>[paralayerCount];

            for (var i = 0; i < _parallaxLayersArray.Length; i++) {
                _parallaxLayersArray[i] = new List<Entity>();
            }
        }

        // Organize entities in the correct ParallaxLayer bin
        private void AddEntitiesToParallaxLayers(Entity ent)
        {
            if (_parallaxLayersArray != null) {
                _parallaxLayersArray[ent.DrawComponent.ParallaxLayer].Add(ent);
            }
        }
        #endregion
    }
}