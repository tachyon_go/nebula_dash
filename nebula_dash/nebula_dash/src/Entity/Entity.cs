namespace nebula_dash
{
    /// <summary>
    /// Entities are objects that occupy a <see cref="Scene" />
    /// </summary>
    public class Entity
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public Entity()
        {
            InstanceID = 0;
            Name = "";
            RemoveMeFromList = false;
            PositionComponent = null;
            DrawComponent = null;
        }
        #endregion

        #region Properties
        /// <summary>
        /// UniqueID assigned by an <see cref="EntityManager" />
        /// </summary>
        public int InstanceID { get; set; }

        /// <summary>
        /// Entity's friendly name *NOTE: Different from the "Key" value in ent_data
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Set to true when an Entity should be removed from a Scene
        /// </summary>
        public bool RemoveMeFromList { get; set; }

        #region Components
        /// <summary>
        /// Stores data about an Entity's position
        /// </summary>
        public PositionEntityComponent PositionComponent { get; set; }

        /// <summary>
        /// Stores data determining how an Entity should be drawn
        /// </summary>
        public DrawEntityComponent DrawComponent { get; set; }
        #endregion

        #endregion
    }
}