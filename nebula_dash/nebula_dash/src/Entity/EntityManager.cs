#region Using Statements
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Spawns and removes entities for Scenes
    /// </summary>
    public class EntityManager
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public EntityManager()
        {
            _componentFactory = new ComponentFactory();
            ActiveEntities = new List<Entity>();
            _entityMap =
                JsonConvert.DeserializeObject<Dictionary<string, Entity>>(
                    System.IO.File.ReadAllText("data/ent_data.neb"));
            _entityIDCounter = 0;
        }
        #endregion

        #region Fields
        private readonly ComponentFactory _componentFactory;
        private readonly Dictionary<string, Entity> _entityMap;
        private static int _entityIDCounter;
        #endregion

        #region Properties
        /// <summary>
        /// List of entity objects currently occupying the scene
        /// </summary>
        public List<Entity> ActiveEntities { get; }
        #endregion

        #region Public Functions
        /// <summary>
        /// Adds a new entity to <see cref="ActiveEntities" />, either from the ent_data lookup-table,
        /// or as a prototype of another entity (depending on arguments)
        /// </summary>
        /// <param name="entID">String ID of entity in ent_data.neb (leave blank if you don't want to spawn from LUT)</param>
        /// <param name="bEntity">Prototype entity to be cloned, leave null if you are spawning from LUT</param>
        /// <returns></returns>
        public Entity SpawnEntity(string entID = "", Entity bEntity = null)
        {
            var baseEntity = new Entity();

            if (bEntity != null) {
                baseEntity = bEntity;
            } else {
                try {
                    baseEntity = _entityMap[entID];
                } catch (System.Exception) {
                    NebulaDebug.ExceptionMessage("Can't spawn something from ent_data.neb, probably the " +
                                                 "ID name is invalid.\n ID = " + entID);
                    Debugger.Break();
                    baseEntity = _entityMap[""];
                    return baseEntity;
                }
            }

            var ent = new Entity {InstanceID = _entityIDCounter};
            _entityIDCounter++;

            try {
                ent.Name = string.Copy(baseEntity.Name);
            } catch (System.Exception) {
                NebulaDebug.ExceptionMessage("Entity (Instance ID #" + ent.InstanceID +
                                             ") needs a name.");
                Debugger.Break();
            }

            _componentFactory.CopyComponents(baseEntity, ent);
            ActiveEntities.Add(ent);
            return ent;
        }

        /// <summary>
        /// Iterates through a list of Entity objects, and runs SpawnEntity(ent) for each element
        /// </summary>
        /// <param name="entities"></param>
        public void SpawnEntitiesFromList(List<Entity> entities)
        {
            foreach (Entity ent in entities) {
                SpawnEntity("", ent);
            }
        }

        /// <summary>
        /// Remove an entity from a scene, sets ent.RemoveMeFromList to true
        /// </summary>
        /// <param name="entInstanceID">Entity to be removed's InstanceID property</param>
        public void MarkEntityForRemoval(int entInstanceID)
        {
            foreach (Entity ent in ActiveEntities) {
                if (ent.InstanceID == entInstanceID) {
                    ent.RemoveMeFromList = true;
                }
            }
        }

        /// <summary>
        /// Removes all entity objects marked for removal
        /// </summary>
        public void RemoveMarkedEntities()
        {
            var entCleanup = new List<Entity>(ActiveEntities);
            foreach (Entity ent in entCleanup) {
                if (ent.RemoveMeFromList) {
                    ActiveEntities.Remove(ent);
                }
            }
        }
        #endregion
    }
}