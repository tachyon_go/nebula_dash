#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Used primarily by <see cref="DrawSystem" /> objects to draw 2D spritesheet based entities
    /// </summary>
    public class DrawEntityComponent : INebulaComponent
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawEntityComponent()
        {
            InstanceID = 0;
            Scale = Point.Zero;
            TintColor = Color.White;
            Rotation = 0.0f;
            Origin = Vector2.Zero;
            Flip = SpriteEffects.None;
            Layer = 0.0f;
            ParallaxLayer = 0;
            CurrentSprite = new Sprite();
            NextSprite = new Sprite();
            IdleSprite = new Sprite();
            WalkSprite = new Sprite();
        }
        #endregion

        #region Copy Constructor
        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="bComponent">Base component to use for new component's properties</param>
        public DrawEntityComponent(DrawEntityComponent bComponent)
            : this()
        {
            Scale = bComponent.Scale;
            TintColor = bComponent.TintColor;
            Rotation = bComponent.Rotation;
            Origin = bComponent.Origin;
            Flip = bComponent.Flip;
            Layer = bComponent.Layer;
            ParallaxLayer = bComponent.ParallaxLayer;
            CurrentSprite = new Sprite(bComponent.CurrentSprite);
            NextSprite = new Sprite(CurrentSprite);
            IdleSprite = new Sprite(bComponent.IdleSprite);
            WalkSprite = new Sprite(bComponent.WalkSprite);
        }
        #endregion

        #region Properties
        /// <summary>
        /// UniqueID assigned by a ComponentFactory
        /// </summary>
        public int InstanceID { get; set; }

        /// <summary>
        /// Sprite size in x,y format
        /// </summary>
        public Point Scale { get; set; }

        /// <summary>
        /// RGBA color struct used in Draw()
        /// </summary>
        public Color TintColor { get; set; }

        /// <summary>
        /// Radian rotation of the sprite
        /// </summary>
        public float Rotation { get; set; }

        /// <summary>
        /// Origin point for rotation operations
        /// </summary>
        public Vector2 Origin { get; set; }

        /// <summary>
        /// Sprite orientation
        /// </summary>
        public SpriteEffects Flip { get; set; }

        /// <summary>
        /// 0(front) to 1(back) layer ordering
        /// </summary>
        public float Layer { get; set; }

        /// <summary>
        /// Parallax Layer the sprite belongs to, see <see cref="Scene" /> documentation for more info
        /// </summary>
        public int ParallaxLayer { get; set; }

        #region Sprites
        /// <summary>
        /// Sprite currently being drawn
        /// </summary>
        public Sprite CurrentSprite { get; set; }

        /// <summary>
        /// Next sprite to be drawn
        /// </summary>
        public Sprite NextSprite { get; set; }

        /// <summary>
        /// Idle state sprite
        /// </summary>
        public Sprite IdleSprite { get; set; }

        /// <summary>
        /// Walk state sprite
        /// </summary>
        public Sprite WalkSprite { get; set; }
        #endregion

        #endregion
    }
}