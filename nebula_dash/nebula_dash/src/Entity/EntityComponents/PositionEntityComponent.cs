#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace nebula_dash
{
    /// <summary>
    /// Holds entity world position data
    /// </summary>
    public class PositionEntityComponent : INebulaComponent
    {
        #region Default Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public PositionEntityComponent()
        {
            InstanceID = 0;
            Position = Vector2.Zero;
        }
        #endregion

        #region Copy Constructor
        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="bComponent">Base component to use for new component's properties</param>
        public PositionEntityComponent(PositionEntityComponent bComponent)
            : this()
        {
            Position = bComponent.Position;
        }
        #endregion

        #region Properties
        /// <summary>
        /// UniqueID assigned by a ComponentFactory
        /// </summary>
        public int InstanceID { get; set; }

        /// <summary>
        /// World-coordinate X,Y position
        /// </summary>
        public Vector2 Position { get; set; }
        #endregion
    }
}